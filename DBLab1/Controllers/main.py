from Models.plane_table import PlaneTable
from Models.flight_table import FlightTable
from Views.command_parser import *


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


class Main:

    def __init__(self):
        self.tours = FlightTable([])
        self.firms = PlaneTable([], self.tours)

    def init_tables(self):
        self.firms.load(get_filename("planes"))
        self.tours.load(get_filename("flights"))

    def run(self):
        while True:
            print('Enter command, please. Commands: update, get, filter, all, delete, create, save')
            command = validate_command(get_command())
            if command == Commands.GET_COMMAND:
                self.get()
            elif command == Commands.DELETE_COMMAND:
                self.delete()
            elif command == Commands.CREATE_COMMAND:
                self.create()
            elif command == Commands.FILTER_COMMAND:
                self.filter()
            elif command == Commands.ALL_STATEMENT:
                self.all()
            elif command == Commands.SAVE_COMMAND:
                self.firms.save()
                self.tours.save()
            elif command == Commands.UPDATE_COMMAND:
                self.update()

    def update(self):
        try:
            print('enter type plane/flight')
            type = get_command()
            print('enter id')
            id = get_command()
            print('enter name')
            name = get_command()
            if type == 'plane':
                input_object = {
                    'type': type,
                    'id': id,
                    'name': name
                }
            if type == 'flight':
                print('enter place')
                place = get_command()
                print('enter plane id')
                firm_id = int(get_command())
                input_object = {
                    'type': type,
                    'id': id,
                    'name': name,
                    'place': place,
                    'plane_id': firm_id
                }
            type = input_object['type']
            id = input_object['id']
            obj = None
            if type == 'plane':
                if is_int(id):
                    obj = self.firms.get(id=int(id))
                else:
                    obj = self.firms.get(name=id)
                obj.name = input_object['name']
            elif type == 'flight':
                if is_int(id):
                    obj = self.tours.get(id=int(id))
                else:
                    obj = self.tours.get(name=id)
                obj.name = input_object['name']
                obj.place = input_object['place']
                obj.firm_id = input_object['plane_id']
            view_data(obj)
        except Exception as e:
            print(e.args)

    def get(self):
        try:
            print('enter type plane/flight')
            command = get_command()
            print('enter name or id')
            id = get_command()
            if command == 'plane':
                type, id = command, id
            elif command == 'flight':
                type, id = command, id
            obj = None
            if type == 'plane':
                if is_int(id):
                    obj = self.firms.get(id=int(id))
                else:
                    obj = self.firms.get(name=id)
            elif type == 'flight':
                if is_int(id):
                    obj = self.tours.get(id=int(id))
                else:
                    obj = self.tours.get(name=id)

            if obj is None:
                raise Exception("can't find " + id)
            view_data(obj)
        except Exception as e:
            print(e.args)

    def filter(self):
        try:
            place = input("Enter place\n")
            objects = self.tours.filter(lambda item: item.place == place)
            view_list(objects)
        except Exception as e:
            print(e.args)

    def all(self):
        try:
            type = input("Enter what you wanna see\n")
            if type == 'planes':
                view_list(self.firms.objects)
            elif type == 'flights':
                view_list(self.tours.objects)


        except Exception as e:
            print(e.args)

    def delete(self):
        try:
            print('enter type plane/flight')
            command = get_command()
            print('enter name or id')
            id = get_command()
            if command == 'plane':
                type, id = command, id
            elif command == 'flight':
                type, id = command, id
            obj = None
            if type == 'plane':
                if is_int(id):
                    obj = self.firms.get(id=int(id))
                else:
                    obj = self.firms.get(name=id)
                self.firms.delete(obj)
            elif type == 'flight':
                if is_int(id):
                    obj = self.tours.get(id=int(id))
                else:
                    obj = self.tours.get(name=id)
                self.tours.delete(obj)
            if obj is None:
                raise Exception("can't find " + id)
            view_data(obj)
        except Exception as e:
            print(e.args)

    def create(self):
        try:
            print('enter type plane/flight')
            type = get_command()
            print('enter name')
            name = get_command()
            if type == 'plane':
                input_object = {
                    'type': type,
                    'name': name
                }
            if type == 'flight':
                print('enter place')
                place = get_command()
                print('enter plane id')
                firm_id = int(get_command())
                input_object = {
                    'type': type,
                    'name': name,
                    'place': place,
                    'plane_id': firm_id
                }
            type = input_object['type']
            if type == 'plane':
                self.firms.create(input_object['name'])
            elif type == 'flight':
                self.tours.create(input_object['name'], input_object['place'], input_object['plane_id'])
            else:
                raise Exception()
        except Exception as e:
            print(e.args)




main = Main()
main.init_tables()
main.run()
#flights = FlightTable([])
#planes = PlaneTable([], flights)
#planes.create("name")
#flights.create("name", "place", 0)
#planes.save("planes")
#flights.save("flights")
