from .printable import Printable


class Plane(Printable):

    def __init__(self, name, id):
        Printable.__init__(self)
        self.name = name
        self.id = id
        self.flights = None

    def dict(self):
        return {
            'name': self.name,
            'id': self.id,
            'flights': self.flights
        }
