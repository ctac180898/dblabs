from .table import Table
from .plane import Plane


class PlaneTable(Table):

    def __init__(self, objects, flight_table, filename=None):
        Table.__init__(self, objects, filename)
        self.flights = flight_table

    def get(self, name=None, id=None):
        if name is not None:
            for obj in self.objects:
                if obj.name == name:
                    obj.flights = [item.id for item in self.flights.filter(lambda item: item.plane_id == obj.id)]
                    return obj
        elif id is not None:
            for obj in self.objects:
                if obj.id == id:
                    obj.flights = [item.id for item in self.flights.filter(lambda item: item.plane_id == obj.id)]
                    return obj

    def create(self, name):
        plane = Plane(name, self.id)
        self.id += 1
        self.objects.append(plane)

    def delete(self, obj):
        flights = self.flights.filter(lambda item: item.plane_id == obj.id)
        for flight in flights:
            self.flights.delete(flight)
        self.objects.remove(obj)

