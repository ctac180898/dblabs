from .table import Table
from .flight import Flight


class FlightTable(Table):

    def __init__(self, objects, filename=None):
        Table.__init__(self, objects, filename)

    def get(self, name=None, id=None):
        if name is not None:
            for obj in self.objects:
                if obj.name == name:
                    return obj
        elif id is not None:
            for obj in self.objects:
                if obj.id == id:
                    return obj

    def create(self, name, place, plane_id):
        flight = Flight(name, place, self.id, plane_id)
        self.id += 1
        self.objects.append(flight)

    def delete(self, obj):
        self.objects.remove(obj)
