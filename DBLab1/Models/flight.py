from .printable import Printable


class Flight(Printable):

    def __init__(self, name, place, id, plane_id):
        Printable.__init__(self)
        self.name = name
        self.place = place
        self.id = id
        self.plane_id = plane_id

    def dict(self):
        return {
            'name': self.name,
            'id': self.id,
            'place': self.place,
            'plane_id': self.plane_id
        }

